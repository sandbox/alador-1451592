(function($) {
	Drupal.behaviors.horizontal_loginbar = {
    attach: function (context, settings) {
	  //Retrieve module configuration send by hook_init()
      var enableNameEvent = Drupal.settings.horizontal_loginbar_settings.name_event;
      var enablePassEvent = Drupal.settings.horizontal_loginbar_settings.pass_event;
      var enableFocusEvent = Drupal.settings.horizontal_loginbar_settings.focus_event;
      var displayLabels = Drupal.settings.horizontal_loginbar_settings.display_labels;
      
      /*
       * Turn ON/OFF Labels 
       */
        
      if (displayLabels == 0) {
		var $label = null;
		
		if ($('.login-name-input').prev().attr('for') == 'edit-name') {
			$label = $('.login-name-input').prev();
			$label.addClass('hidden');
		}
		
		if ($('.login-pwd-input').prev().attr('for') == 'edit-pass') {
			$label = $('.login-pwd-input').prev();
			$label.addClass('hidden');
		}
		
	  }
      
      /*
       * Click() and Blur() events on Name Input (#edit-name)
       * 	- Clear textfield on click & retrieve datas on blur if field is empty
       *	- Remove Focus() classes on Blur()
       * 	- Can be disabled on the module configuration page
       */
      var valName = null;
     
      if (enableNameEvent == 1) {
		$('.login-name-input', context).click(function () {
			if (this.value != '') {
				valName = this.value;
				this.value = '';
				//If class doesn't exist already
				if (!$(this).hasClass('hlogin-focus')){
					$(this).addClass('hlogin-focus');
				}
			}
		});
	  	  
		$('.login-name-input', context).blur(function () {
			if (this.value == '') {
				this.value = valName;
			}
			$(this).removeClass('hlogin-focus');
		});
      }
      
      /*
       * Click() and Blur() events on Pass Input (#edit-pass)
       * 	- Clear textfield on click & retrieve datas on blur if field is empty
       *	- Remove Focus() classes on Blur()
       * 	- Can be disabled on the module configuration page
       */
      var valPass = null;
            
      if (enablePassEvent == 1) {		
		$('.login-pwd-input', context).click(function () {
			if (this.value != '') {
				valPass = this.value;
			}
			this.value = '';
			//If class doesn't exist already
			if (!$(this).hasClass('hlogin-focus')){
				$(this).addClass('hlogin-focus');
			}
		});
	  	  
		$('.login-pwd-input', context).blur(function () {
			if (this.value == '') {
				this.value = valPass;	
			}
			$(this).removeClass('hlogin-focus');
		});
	  }
	  
	  /*
	   * Focus() event on Name & Pass Input (#edit-name) & (#edit-pass)
	   * 	- Add CSS class
	   */
	   if (enableFocusEvent == 1) {
			$('.login-name-input', context).focus(function () {
				$(this).addClass('hlogin-focus');
			});
			
			$('.login-pwd-input', context).focus(function () {
				$(this).addClass('hlogin-focus');
			});		
	   } 
      
    }
  };


})(jQuery);
